DROP DATABASE `practice8_db`;


CREATE DATABASE `practice8_db`;
USE `practice8_db`;


CREATE TABLE `users` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
	`login` VARCHAR(15) UNIQUE NOT NULL
);

CREATE TABLE `teams` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
	`name` VARCHAR(15) UNIQUE NOT NULL
);

CREATE TABLE `users_teams` (
	`user_id` INT NOT NULL REFERENCES `users`.id ON DELETE CASCADE,
	`team_id` INT NOT NULL REFERENCES `teams`.id ON DELETE CASCADE,
	PRIMARY KEY (`user_id`,`team_id`)
);


INSERT INTO `users` VALUES (DEFAULT, 'ivanov');

INSERT INTO `teams` VALUES (DEFAULT, 'teamA');
