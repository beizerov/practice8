package ua.nure.beizerov.practice8.db.entity;


public class User {
	
	private int id;
	private String login;
	
	
	private User() {
		
	}
	
	
	public static User createUser(String login) {
		User user = new User();
		user.setLogin(login);
		
		return user;
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		
		User other = (User) obj;
		
		return (login == null) 
				? (other.login != null) : login.equals(other.login);
	}
	
	
	@Override
	public String toString() {
		return login;
	}
}
