package ua.nure.beizerov.practice8.db.entity;


public class Team {

	private int id;
	private String name;

	
	private Team() {
		
	}
	
	
	public static Team createTeam(String name) {
		Team team = new Team();
		team.setName(name);
		
		return team;
	}
	
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		
		Team other = (Team) obj;
		
		return (name == null) ? (other.name != null) : name.equals(other.name);
	}
	
	
	@Override
	public String toString() {
		return name;
	}
}
