package ua.nure.beizerov.practice8.db;


import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import ua.nure.beizerov.practice8.db.entity.Team;
import ua.nure.beizerov.practice8.db.entity.User;


public class DBManager {
	
	private static DBManager dbManager;
	private static final String CONNECTION_URL;
	
	private static final String SQL_CREATE_USER;
	private static final String SQL_CREATE_TEAM;
	
	private static final String SQL_FIND_ALL_USERS;
	private static final String SQL_FIND_ALL_TEAMS;
	
	private static final String SQL_GET_USER_BY_LOGIN;
	private static final String SQL_GET_TEAM_BY_NAME;
	
	private static final String SQL_SET_TEAM_FOR_USER;
	
	private static final String SQL_GET_TEAMS_OF_USER;
	
	private static final String SQL_DELETE_TEAM;
	
	private static final String SQL_UPDATE_TEAM;
	
	
	static {
		Properties config = new Properties();
		try (FileInputStream fInputStream = new FileInputStream(
				"app.properties"
		)) {		
			config.load(fInputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		CONNECTION_URL = config.getProperty("connection.url");
		
		SQL_CREATE_USER = "INSERT INTO users VALUES (DEFAULT, ?)";
		SQL_CREATE_TEAM = "INSERT INTO teams VALUES (DEFAULT, ?)";
		
		SQL_FIND_ALL_USERS = "SELECT * FROM users";
		SQL_FIND_ALL_TEAMS = "SELECT * FROM teams";
		
		SQL_GET_USER_BY_LOGIN = "SELECT * FROM users WHERE login = ?";
		SQL_GET_TEAM_BY_NAME = "SELECT * FROM teams WHERE name = ?";
		
		SQL_SET_TEAM_FOR_USER = "INSERT INTO users_teams VALUES (?, ?)";
		
		SQL_GET_TEAMS_OF_USER = "SELECT * "
				+ "FROM users_teams ut RIGHT JOIN teams t "
				+ "ON ut.team_id = t.id "
				+ "WHERE ut.user_id = ?";
		
		SQL_DELETE_TEAM = "DELETE FROM teams WHERE name = ?";
		
		SQL_UPDATE_TEAM = "UPDATE teams " 
						+ "set name = ? "
						+ "WHERE id = ?";
	}
			
	
	private DBManager() {
		
	}
	
	
	public static synchronized DBManager getInstance() {
		return (dbManager == null) ? new DBManager() : dbManager;
	}


	public User insertUser(User user) {
		try (Connection conn = DriverManager.getConnection(CONNECTION_URL)
			; PreparedStatement pstmt = conn
				.prepareStatement(
					SQL_CREATE_USER, Statement.RETURN_GENERATED_KEYS
				)
		) {
			pstmt.setString(1, user.getLogin());
			
			if (pstmt.executeUpdate() <= 0) { 
				throw new SQLException("User not created!");
			}
			
			try (ResultSet rs = pstmt.getGeneratedKeys()) {
				if (rs.next()) {
					int userId = rs.getInt(1);
					user.setId(userId);
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return user;
	}
	
	public Team insertTeam(Team team) {
		try (Connection conn = DriverManager.getConnection(CONNECTION_URL)
			; PreparedStatement pstmt = conn
				.prepareStatement(
					SQL_CREATE_TEAM, Statement.RETURN_GENERATED_KEYS
				)
		) {
			pstmt.setString(1, team.getName());
			
			if (pstmt.executeUpdate() <= 0) { 
				throw new SQLException("Team not created!");
			}
			
			try (ResultSet rs = pstmt.getGeneratedKeys()) {
				if (rs.next()) {
					int teamId = rs.getInt(1);
					team.setId(teamId);
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return team;
	}

	public List<User> findAllUsers() {
		List<User> users = new ArrayList<>();
		try (Connection conn = DriverManager.getConnection(CONNECTION_URL)
			; Statement stmt = conn.createStatement()
			; ResultSet rs = stmt.executeQuery(SQL_FIND_ALL_USERS)
		) {
			while (rs.next()) {
				users.add(extractUser(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return users;
	}
	
	public List<Team> findAllTeams() {
		List<Team> teams = new ArrayList<>();
		try (Connection conn = DriverManager.getConnection(CONNECTION_URL)
			; Statement stmt = conn.createStatement()
			; ResultSet rs = stmt.executeQuery(SQL_FIND_ALL_TEAMS)
		) {
			while (rs.next()) {
				teams.add(extractTeam(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return teams;
	}

	
	private static User extractUser(ResultSet rs) throws SQLException {
		User user = User.createUser(rs.getString("login"));
		user.setId(rs.getInt("id"));

		return user;
	}
	
	private static Team extractTeam(ResultSet rs) throws SQLException {
		Team team = Team.createTeam(rs.getString("name"));
		team.setId(rs.getInt("id"));
		
		return team;
	}


	public User getUser(String login) {
		User user = null;
		try (Connection conn = DriverManager.getConnection(CONNECTION_URL)
			; PreparedStatement pstmt = conn
					.prepareStatement(
						SQL_GET_USER_BY_LOGIN
					)
		) {
			pstmt.setString(1, login);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				if (rs.next()) {
					user = extractUser(rs);
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return user;
	}

	public Team getTeam(String name) {
		Team team = null;
		try (Connection conn = DriverManager.getConnection(CONNECTION_URL)
			; PreparedStatement pstmt = conn
					.prepareStatement(
						SQL_GET_TEAM_BY_NAME
					)
		) {
			pstmt.setString(1, name);
			
			try (ResultSet rs = pstmt.executeQuery()) {
				if (rs.next()) {
					team = extractTeam(rs);
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return team;
	}
	
	public void setTeamsForUser(User user, Team... teams) {
		Connection conn = null;
		
		try {
			conn = DriverManager.getConnection(CONNECTION_URL);
			conn.setAutoCommit(false);
			
			try (PreparedStatement pstmt = conn.prepareStatement(
					SQL_SET_TEAM_FOR_USER
			)) {
				for (int i = 0; i < teams.length; i++) {
					pstmt.setInt(1, user.getId());
					pstmt.setInt(2, teams[i].getId());
					
					pstmt.execute();	
				}
			}
			
			conn.commit();
		} catch (SQLException e) {
			if (conn != null) {
				try {
					conn.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.setAutoCommit(true);
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public List<Team> getUserTeams(User user) {
		List<Team> teams = new ArrayList<>();
		try (Connection conn = DriverManager.getConnection(CONNECTION_URL)
			; PreparedStatement pstmt = conn
					.prepareStatement(
						SQL_GET_TEAMS_OF_USER
					)
		) {
			pstmt.setInt(1, user.getId());
			
			try (ResultSet rs = pstmt.executeQuery()) {
				while (rs.next()) {
					teams.add(extractTeam(rs));
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return teams;
	}

	public void deleteTeam(Team team) {
		try (Connection conn = DriverManager.getConnection(CONNECTION_URL)
			; PreparedStatement pstmt = conn
					.prepareStatement(
						SQL_DELETE_TEAM
					)
		) {
			pstmt.setString(1, team.getName());
			pstmt.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void updateTeam(Team team) {
		try (Connection conn = DriverManager.getConnection(CONNECTION_URL)
			; PreparedStatement pstmt = conn
					.prepareStatement(
						SQL_UPDATE_TEAM
					)
		) {
			pstmt.setString(1, team.getName());
			pstmt.setInt(2, team.getId());
			
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
